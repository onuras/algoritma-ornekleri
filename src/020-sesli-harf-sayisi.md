# Sesli Harf Sayısı

Bu örnek yazı işlemeye giriş niteliğindedir. Bize bir kaç satır yazı verilecek
ve her bir satır içerisinde yer alan sesli harflerin
(`a`, `o`, `u`, `i`, `e`, `y`) sayısını bulacağız. **Not**: `y` harfi bu örnek
için sesli harf sayılmıştır.

**Girdi verisi:** İlk satır toplam test sayısını vermektedir.<br>
Ardıdan gelen her bir satır sesli harflerin sayısını bulacağınız
yazıyı içermektedir.<br>
Her bir satır sadece küçük harf Latin karakterler ve boşluklar içermektedir.<br>
**Sonuç:** Her bir satırdaki sesli harf sayısını aralarında boşluk olacak şekilde
vermelidir.


Örnek:

```
girilen değer:
4
abracadabra
pear tree
o a kak ushakov lil vo kashu kakao
my pyx

sonuç:
5 4 13 2
```
