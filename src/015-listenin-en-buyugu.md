# Listenin En Büyüğü

Bu problem, programlamada daha karmaşık alanlarda (sıralama vs.) kullanılarak
öğrenilen, popüler "çizgisel arama" (linear search) algoritmasını öğretmektedir.

Bir dizi değere sahip bir liste veya arrayde minimum veya
maksimum değerleri bulma en sık yapılan işlemlerden biridir. Bunu
başarabilmek için programcının **şimdiki maksimum** (veya minimum) değeri,
farklı bir değişken içerisinde tutması ve liste elemanlarının her birini
bu değer ile karşılaştırması gerekir. Bir sonraki değer, bu geçici değerden
büyükse, yeni değer artık en büyük kabul edilmeli ve bu değişkene
atanmalıdır.

Döngünün sonunda bu geçici değer en büyük değeri tutacaktır.

**Girdi verisi:** Tek bir satırda 300 sayı verilecektir.<br>
**Sonuç:** Girilen sayıların en büyüğü ve en küçüğü aralarında boşluk olacak
şekilde gösterilmelidir.

Örnek:

```
girilen değer:
1 3 5 7 9 11 ... 295 297 299 300 298 296 ... 12 10 8 6 4 2

sonuç:
300 1
```
