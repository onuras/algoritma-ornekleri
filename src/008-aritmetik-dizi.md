# Aritmetik Dizi

Ardışık iki terimi arasındaki fark sabit olan dizilere aritmetik dizi denir.
Bu sabit farka ortak fark denir.

`(K+1)`'inci ve `K` sabitli diziler olmak üzere bir kaç örnek:

```
1 2 3 4 5 6 7 ...
4 6 8 10 12 14 16...
10 13 16 19 22 25 28...
```

Bu nedenle aritmetik diziler ilk elemanı (`A`) ve artan farkı - ortak sabit (`B`)
ile tanımlanır. İlk elemanlar şu şekilde ifade edilebilir:

```
A + (A + B) + (A + 2B) + (A + 3B) + ...
```

Sizden bir aritmetik dizinin elemanları toplamını hesaplamanızı istiyoruz.

**Girdi verisi:** İlk satır hesaplamanızı gerektiren toplam test sayısını
vermektedir.<br>
Diğer satır `A B N` üçlülerini vermektedir. `A` ilk eleman, `B` ortak fark
ve `N` dizide yer alan eleman sayısını vermektedir.<br>
**Cevap:** Dizinin elemanları toplamını aralarında boşluk olacak şekilde
ekrana yazdırın.

Örnek:

```
girilen değer:
2
5 2 3
3 0 10

sonuç:
21 30
```

Örnek açıklaması: İlk örnekte başlangıç elemanımız `5` ve ortak fark `2`. Bizden
bu dizideki `3` elemanın toplamını soruyor: `5 + 7 + 9 = 21`. İkinci örnek daha
basit: Bizden `3` ile başlamamızı ve diziyi `0` ile arttırmamızı istiyor. Yani
`10` elemanlı bu dizinin elemanları toplamı: `3 + 3 + ... + 3 = 30` 
