# Rakamların Ağırlıklı Toplamı

Bu örnek; CRC ve diğer checksum hesaplamaları ve aynı zamanda yazı
karakterlerinden hesaplanan hash değerleri gibi daha karmaşık
algoritmalara benzemektedir. Aynı zamanda sayıları, sayı değerlerine bölmek
için bir başka örnektir. Daha önce
[sayı değerleri toplamı](011-sayi-degerleri-toplami.md) örneğine bakabilirsiniz.

Önceki örnekte olduğu gibi sayı değerleri toplamını hesaplayalım,
fakat bu sefer her bir rakamı, en soldaki 1 olmak üzere birer arttırarak
çarpalım. Örneğin, `1776` sayısının rakamlarının ağırlıklı toplamını
hesaplamak için (fonksiyonumuzun adı "rat" olsun):

```
rat(1776) = 1 * 1 + 7 * 2 + 7 * 3 + 6 * 4 = 60
```

**Girdi verisi:** İlk satır hesaplamanızı gerektiren toplam test sayısını
vermektedir.<br>
Hesaplamanız gereken değerler ikinci satırda aralarında bir boşluk olacak
şekilde yer almaktadır.<br>
**Cevap:** Sayıların, rakamları ağırlıklı toplamını aralarında boşluk olacak
şekilde ekrana yazdırın.


Örnek:

```
girilen değer:
3
9 15 1776

sonuç:
9 11 60
```
