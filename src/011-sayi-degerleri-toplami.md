# Sayı Değerleri Toplamı

Bu örnek sayı sistemlerinin temellerini öğretmeyi amaçlamaktadır. Bu kavramı
gündelik yaşantımızda kullandığımız onluk sayı sistemiyle oynayarak
öğreneceğiz (unutmayın ki bilgisayar onluk sayı sistemini kullanmaz, sadece
kullanıcıya gösterirken onluk tabana çevirir).

9 dan daha büyük bir sayı birden fazla rakam kullanılarak yazılır ve biz bu
rakamların toplamını bulabiliriz. Örneğin `1492` ve `1776` sayıları için:

```
1 + 4 + 9 + 2 = 16
1 + 7 + 7 + 6 = 21
```

Bu problemde size bazı sayıları vereceğiz ve bu sayıları oluşturan rakamların
toplamını bulmanızı istiyoruz.

**Önemli:** Bir çok programlama dili sayıları yazıya çeviren fonksiyonlar
içermektedir, bu tür fonksiyonları kullanmamalısınız.

**Yazı olarak kullanmak yerine**, sayıyı sürekli 10'a bölüp kalan ile toplayan
bir algoritma geliştirmelisiniz.

**Girdi verisi:**

* İlk satır `N` - işlemeniz gereken toplam test sayısını vermektedir.
* Ardından gelen her bir `N` satır 3 tane sayı içermektedir: `A B C`.
* Her bir satır için `A` ile `B`'yi çarpıp `C` ile toplamanızı istiyoruz, yani
  sonuç şu olmalı: `A * B + C`, ardıdan çıkan sonucun rakamlarını toplayın.

Örnek:

```
girilen değer:
3
11 9 1
14 90 232
111 15 111

sonuç:
1 16 21
```

İlk durum `11*9+1 = 100` işlemini hesaplamayı ve rakamları toplamı: `1+0+0 = 1`
bulmayı gerektirir.
